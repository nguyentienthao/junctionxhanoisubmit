
userInfor = null;
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var flash = require('connect-flash');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var session = require('express-session');
var cors = require('cors');
// var http = require('http').createServer(app);
// var io = require('socket.io')(http);
// io.on('connection', function(socket){
//   console.log('a user connected');
// });

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

app = express();
app.use(cors());

http = require('http').createServer(app);
io = require('socket.io')(http);

// console.log(io.sockets);
// io.sockets.on('connection', function (socket) {
//   console.log('lol')
//   socket.emit('news', { hello: 'world' });
//   socket.on('my other event', function (data) {
//     console.log(data);
//   });
// });

var indexRouter = require('./routes/index');
var agenciesRouter = require('./routes/agency');
var customersRouter = require('./routes/customer');
var adminRouter = require('./routes/admin');

require('./config/passport.serialize');
require('./config/passport.deserialize');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/customer',express.static(path.join(__dirname, 'public')));
app.use('/agency',express.static(path.join(__dirname, 'public')));
app.use('/agency/history',express.static(path.join(__dirname, 'public')));
app.use('/admin',express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use('/', indexRouter);
app.use('/agency', agenciesRouter);
app.use('/customer', customersRouter);
app.use('/admin', adminRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
http.listen(3000);
module.exports = app;
