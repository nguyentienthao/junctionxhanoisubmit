const Tx = require('ethereumjs-tx');

module.exports = function(web3, Contract){
    const utils = {
        totalSupply() {
            return new Promise((resolve, reject) => {
                Contract.methods.totalSupply().call({}, function(error, total){
                    console.log("----------totalSupply---------");
                    if(!error){
                        resolve(web3.utils.fromWei(balance, "ether"));
                    }else{
                        reject(error);
                    }
                });
            });
        },
        balanceOf(address) {
            return new Promise((resolve, reject) => {
                Contract.methods.balanceOf(address).call({}, function(error, balance){
                    console.log("----------balanceOf---------");
                    if(!error){
                        resolve(web3.utils.fromWei(balance, "ether"));
                    }else{
                        reject(error);
                    }
                });
            });
        },
        transfer(fromObj, toAddress, value, contractAddress) {
            console.log("----------transfer---------");
            data = Contract.methods.transfer(toAddress, web3.utils.toWei('' + value, 'ether')).encodeABI();
            return this.sendSignedTransaction(fromObj, contractAddress, data);

        },
        approve(fromObj, spender, value, contractAddress) {
            console.log("----------approve---------");
            data = Contract.methods.approve(spender, web3.utils.toWei('' + value, 'ether')).encodeABI();
            return this.sendSignedTransaction(fromObj, contractAddress, data);
        },
        transferFrom(fromObj, transferFromAddress, toAddress, value, contractAddress) {
            console.log("----------transferFrom---------");
            data = Contract.methods.transferFrom(transferFromAddress, toAddress, web3.utils.toWei('' + value, 'ether')).encodeABI();
            return this.sendSignedTransaction(fromObj, contractAddress, data);
        },
        allowance(ownerAddress, spenderAddress){
            console.log("----------allowance---------");
            return new Promise((resolve, reject) => {
                 data = Contract.methods.allowance(ownerAddress, spenderAddress).call().then(allowed => {
                    resolve(web3.utils.fromWei(allowed, "ether"));
                });
            });

        },
        burn(fromObj, value, contractAddress){
            console.log("----------burn---------");
            data = Contract.methods.burn(web3.utils.toWei('' + value, 'ether')).encodeABI();
            this.sendSignedTransaction(fromObj, contractAddress, data);
        },
        transferAnyERC20Token(fromObj, contractAddress, value) {
            console.log("----------transferAnyERC20Token---------");
            data = Contract.methods.transferAnyERC20Token(contractAddress, web3.utils.toWei('' + value, 'ether')).encodeABI();
            this.sendSignedTransaction(fromObj, contractAddress, data);
        },
        getOwner() {
            console.log("----------getOwner---------");
            return new Promise((resolve, reject) => {
                Contract.methods.owner().call({}).then(owner => {
                    resolve(owner);
                })
            });

        },
        pause(fromObj, contractAddress) {
            console.log("----------pause Transferation---------");
            data = Contract.methods.pause().encodeABI();
            this.sendSignedTransaction(fromObj, contractAddress, data);
        },
        unpause(fromObj, contractAddress) {
            console.log("----------unpause Transferation---------");
            data = Contract.methods.unpause().encodeABI();
            this.sendSignedTransaction(fromObj, contractAddress, data);
        },
        transferOwnership(fromObj, newOwnerAddress, contractAddress) {
            console.log("----------transferOwnership---------");
            data = Contract.methods.transferOwnership(newOwnerAddress).encodeABI();
            this.sendSignedTransaction(fromObj, contractAddress, data);
        },
        acceptOwnership(fromObj, contractAddress) {
            console.log("----------acceptOwnership---------");
            data = Contract.methods.acceptOwnership().encodeABI();
            this.sendSignedTransaction(fromObj, contractAddress, data);
        },
        getNonce(acc) {
            return new Promise((resolve, reject) => {
                web3.eth.getTransactionCount(acc, (error, result) => {
                    if(error) reject(error);
                    resolve(result);
                })
            });
        },
        getGasPrice() {
            return new Promise((resolve, reject) => {
                web3.eth.getGasPrice((error, result) => {
                    if(error) reject(error);
                    resolve(result);
                })
            })
        },
        transferEther(fromObj, toAddress, moreOptions){
            console.log("----------transferEther---------");
            return this.sendSignedTransaction(fromObj, toAddress, 0, moreOptions);
        },
        sendSignedTransaction(fromObj, toAddress, data, moreOptions=0) {
            return Promise.all([this.getNonce(fromObj.address), this.getGasPrice()])
            .then(values => {
                var rawTx = {};
                if(data) rawTx.data = data;
                if(moreOptions.value){
                    rawTx.value = web3.utils.toHex(moreOptions.value);
                }
                if(moreOptions.nonce){
                    rawTx.nonce = web3.utils.toHex(moreOptions.nonce);
                }
                else{
                    rawTx.nonce = web3.utils.toHex(values[0]);
                }

                rawTx.from = fromObj.address;
                rawTx.to = toAddress;
                rawTx.gasLimit = web3.utils.toHex(1000000);
                // rawTx.gasPrice = web3.utils.toHex(40 * 10**9);
                rawTx.gasPrice = web3.utils.toHex(values[1]);
                // console.log("rawTx", rawTx);
                let tx = new Tx(rawTx);
                let privateKeyBuffer = Buffer.from(fromObj.privateKey, 'hex');
                tx.sign(privateKeyBuffer);
                let serializedTx = tx.serialize();
                return web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
            });
        }
    }
    return utils;
}
