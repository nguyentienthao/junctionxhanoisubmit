// console.log(<%- user %>);
$(document).ready(function() {


    // Submit create Agency
    var server_post = "http://10.79.1.213:8080"
	// console.log(<%=user.UserID%>);
	// console.log(<%- user %>);
	document.getElementById('submit_join').onclick = function() {
		var files = document.getElementById('selectFiles').files;
		console.log(files);
		if (files.length <= 0) {
			return false;
		}

		var fr = new FileReader();

		fr.onload = function(e) {
			console.log(e);
			var result = JSON.parse(e.target.result);
			var formatted = JSON.stringify(result, null, 2);
			console.log(formatted)
		/* 		document.getElementById('result').value = formatted; */

		// "community_name": $('#community_name option:selected').text(),

			$.ajax({
				url: server_post+"/admin/createAgency",
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json',
				success: function (data) {
						console.log("data response: " + JSON.stringify(data))
						if (data.status == "success create Agency"){
							$.notify({
								title: '<strong>Success:<br></strong>',
								message: 'Created a new Agency'
							},{
								type: 'success',
								offset:{
									x: 30,
									y: 60
								}
							});
						}else{
							$('#balance').val("")
							$('#agency_name').val("")
							$('#user_name').val("")
							$('#role').val("")
							$('#phone_number').val("")
							$('#description').val("")
							$.notify({
								title: '<strong>Error:<br></strong>',
								message: "Can't create a new Agency"
							},{
								type: 'danger',
								offset:{
									x: 30,
									y: 60
								}
							});
						}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					// alert("some error");
					$.notify({
						title: '<strong>Error:<br></strong>',
						message: "Can't create a new Agency"
					},{
						type: 'danger',
						offset:{
							x: 30,
							y: 40
						}
					});
				},
				data: JSON.stringify(formatted)
			});
		}
			fr.readAsText(files.item(0));
	};



});
