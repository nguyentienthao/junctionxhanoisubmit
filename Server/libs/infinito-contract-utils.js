const { Wallet, CoinType, EthWallet,BchWallet, InfinitoApi, NeoWallet } = require('node-infinito-wallet');


let opts = {
    apiKey: '3689b67e-0753-4574-81df-f0b2aabb86aa',
    secret: 'AjPGs5pkbDJ6xtmnfbilskuZMNkWz4tEcgM1WNOJ95Vje72Ja0v6DQdifgSxr84E',
    baseUrl: 'https://staging-api-testnet.infinitowallet.io',
    logLevel: 'NONE'
 };

const GASLIMIT = 500000;
const GASPRICE = 40 * 10**9;

var api = new InfinitoApi(opts);
var coinAPI = api.ETH;


// wallet.getBalance().then(console.log);

module.exports = function() {
    const utils = {
        async balanceOf(contractAddr, addr) {
            try{
                const result = await coinAPI.getContractBalance(contractAddr, addr);
                return result;
            }catch(error){
                return error;
            }
        },
        createNewAddress(numAddress) {
            let walletConfig = {
                coinType: CoinType.ETH.symbol,
                isTestNet: true
            }
            var addresses = [];
            for (let count = 0; count < numAddress; count++){
                let account = (new EthWallet(walletConfig)).Account;
                addresses.push({
                    privateKey: account.privateKey.substring(2,),
                    address: account.address
                });
            }
            return addresses;
        },
        async transferEther(privateKey, toAddress, value) {
            try{
                let walletConfig = {
                coinType: CoinType.ETH.symbol,
                isTestNet: true,
                privateKey: privateKey
                }
                let wallet = new EthWallet(walletConfig);
                wallet.setApi(api);
                let result = await wallet.send({
                    txParams: {
                        to: toAddress,
                        amount: value,
                        gasLimit: GASLIMIT,
                        gasPrice: GASPRICE
                    },
                    isBroadCast: true
                });
                return result;
            }catch(error){
                return error;
            }

        },
        async transfer(privateKey, toAddress, value, contractAddress) {
            console.log("----------transfer---------");
            try{
                let configs = await this.setConfig(privateKey, contractAddress, 'transfer', ['address', 'uint256'], [toAddress, value]);
                let wallet = configs[0];
                let raxTx = await wallet.createRawTx(configs[1]);
                let result = await wallet.send({
                    raxTx: raxTx,
                    isBroadCast: true
                });
                return result;
            }catch(error){
                return error;
            }
        },
        async approve(privateKey, spender, value, contractAddress){
            console.log("----------approve---------");
            try{
                let configs = this.setConfig(privateKey, contractAddress,
                'approve', ['address', 'uint256'], [spender, value]);
                let wallet = configs[0];
                let result = await wallet.send({
                    txParams: configs[1],
                    isBroadCast: true
                });
                return result;
            }catch(error){
                return error;
            }
        },
        async transferFrom(fromObj, toAddress, value, contractAddress) {
            console.log("----------transferFrom---------");
            try{
                let configs = this.setConfig(fromObj.privateKey, contractAddress,
                'transferFrom', ['address', 'address', 'uint256'], [fromObj.address, toAddress, value]);
                let wallet = configs[0];
                let result = await wallet.send({
                    txParams: configs[1],
                    isBroadCast: true
                });
                return result;
            }catch(error){
                return error;
            }
        },
        async setConfig(privateKey, contractAddress, nameFunc, typeParams, paramsFuncs) {
            let walletConfig = {
                coinType: CoinType.ETH.symbol,
                isTestNet: true,
                privateKey: privateKey
            }
            let wallet = new EthWallet(walletConfig);
            wallet.setApi(api);

            let txParams = {};
            txParams.sc = {};
            // txParams.gasLimit = GASLIMIT;
            // txParams.gasPrice = GASPRICE;
            txParams.sc.contractAddress = contractAddress;
            txParams.sc.nameFunc = nameFunc;
            txParams.sc.typeParams = typeParams;
            txParams.sc.paramsFuncs = paramsFuncs;
            return [wallet, txParams];
        },
        async createRawTx(txParams) {
            try{
                let result = await wallet.createRawTx(txParams);
                return result;
            }catch(error){
                console.log(error);
            }
        },
        async getNonce(address) {
            const result = await coinAPI.getNonce(address);
            return result.data.nonce;
        }
    }
    return utils;
}
