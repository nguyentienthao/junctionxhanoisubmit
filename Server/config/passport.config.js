var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var poolConnection = require('../models/pool.connection');
var bcrypt = require('bcryptjs');
var User = require('../models/User.model');

// Config here
passport.use('local', new LocalStrategy((username, password, next) => {
  console.log('start authen');
  console.log(username);
  poolConnection.getConnection((err, connection) => {
    if(err) return next(err);
    // console.log('connected as id ' + connection.threadId);
    const sellectQuery = 'SELECT * FROM loyalty.Users WHERE UserName = ?;';
    var params = [username];
    // console.log(username);
    connection.query(sellectQuery, params, (err, results, fields) => {
      if (err) {
       console.log(err);

        return next(err);
      };
      console.log(" audsbfabsdfsafsafsafasdf", results);
      console.log("result"+ results);
      console.log(results.length);
      if((!results || results.length) === 0) {
        console.log("error length");
        return next(null, false);
      }
      console.log(results[0]);
      var user = results[0];
      console.log(user);
      // console.log(password);
      userPass = bcrypt.hashSync(user.Password, bcrypt.genSaltSync(8), null);
    //   userPass =
      // console.log(user);
      bcrypt.compare(password, userPass)
      .then((res) => {
        if(res !== true) {
          // console.log('false!!!');
          return next(null, false);
        }

        if(user.Role == '2'){
          queryCustomerInfo = 'SELECT * FROM loyalty.Customers, loyalty.Users WHERE loyalty.Customers.Users_UserID = loyalty.Users.UserID and loyalty.Users.UserID = ? ;'
          const paramCustomers = [user.UserID];
          connection.query(queryCustomerInfo, paramCustomers, (err, results, fields) => {
            if (err) {
              // console.log('error')
              console.log(err);

              return next(err);
            };
            // console.log(results);
            if(!results || results.length === 0) {
              return next(null, false);
            }
            var infoUser = results[0];
            console.log("infoUser",infoUser);
            return next(null, infoUser);
          })
        }
        // delete user.password;
        // console.log('authen success');
        else{
          console.log("user",user);
          return next(null, user);
        }
      })
    })
    // console.log('???');
    connection.release();
  })
}))

passport.serializeUser(function(user, next) {
  next(null, user.UserID);
});

passport.deserializeUser(function(id, next) {
  // console.log('id');
  // console.log(id);
  User.getById(id, (err, user) => {
    if(err) { return next(err) };
    next(null, user);
  })
});

// exports.isAuthenticated = (req, res, next) => {
//   if (req.isAuthenticated()) return next()
//   return res.json({ error_msg: 'Authenticate failed!' });
// };
