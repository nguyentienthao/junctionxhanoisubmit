var poolConnection = require('../models/pool.connection');
var bcrypt = require('bcryptjs');
var User = require('../models/User.model');
var Agency = require('../models/Agency.model');

exports.getInfor = (req, res, next) => {
  console.log('login controller');
  if(!req.user) return res.json({errAuthen: 'Authenticate fail!!!'});
  userInfor = req.user;
  console.log(userInfor);
  console.log('alo')
  res.redirect('/agency');
}
exports.getByAgencyName = (req, res, callback) => {
  // console.log('get infor Agency');
  // console.log(req);
  return new Promise((resolve, reject) => {
     Agency.getByAgencyName(req, (err, agencyInfo) => {
      if(err) { reject(err) };
      resolve(agencyInfo);
    });
  });
}

exports.logout = (req, res) => {
  req.logout();
  res.json({msg: 'You are out of my system!'});
}



