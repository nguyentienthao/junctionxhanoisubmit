var express = require('express');
var router = express.Router();
var passport = require('passport');
var customerController = require('../controllers/Customer.controller');
var userController = require('../controllers/User.controller');
// console.log(io);
// var server = require('http').createServer();
// var io = require('socket.io')(server);
var contractUtils = require('../libs/contract-utils')()
var Transaction = require('../models/Transaction.model');


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Express' });
});

router.get('/history/:id', function(req, res, next) {
  id=req.params.id;
  Transaction.getByUserId(id,0, (err, transactions) => {
   if(err) { throw (err) };
   console.log("tran " + transactions);
   res.json({ transactions: transactions });
  });

});

router.get('/:username',function(req,res,next){
  username = req.params.username;
  infoUser = customerController.getByCustomerName(username)
  .then(infoUser => {
    res.json({ customer: infoUser });
  })
  .catch(err => console.log(err));
});
router.post('/getItemUsingToken', function(req,res,next){
  console.log('getItemUsingTokentest');
  console.log(req.body);
  // contractUtils.transferFromTo(10**8).then(txhash => {
  //   console.log(txhash);
  // })
  // .catch(error => console.log());
  console.log("now  "+ Date.now());
  transaction = [[1, 2, "0x6f9bdcd5c5d3f1b1a96b347ef08ffc43b03cc7a3784f066b93c844baf9a20a9d","12",Date.now()]];
  Transaction.addNewTransaction(transaction, (err, resTran) => {
   if(err) { throw (err) };
   console.log("res insert Transaction" + resTran);
  });

  console.log(req.body);
  status = customerController.getItemUsingToken(req.body)
  .then(status => {
    res.json({ status: status });
  })
  .catch(err => console.log(err));
  }

);
router.post('/login', passport.authenticate('local') , userController.login);
module.exports = router;
