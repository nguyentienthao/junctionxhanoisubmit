/*
 * NB: since truffle-hdwallet-provider 0.0.5 you must wrap HDWallet providers in a 
 * function when declaring them. Failure to do so will cause commands to hang. ex:
 * ```
 * mainnet: {
 *     provider: function() { 
 *       return new HDWalletProvider(mnemonic, 'https://mainnet.infura.io/<infura-key>') 
 *     },
 *     network_id: '1',
 *     gas: 4500000,
 *     gasPrice: 10000000000,
 *   },
 */
// truffle compile
// truffle migrate --network ropsten
// truffle migrate --network development

// truffle console --network ropsten
// truffle console --network development

// truffle exec ../../web3/execution.js -f balanceOf --network development

var HDWalletProvider = require("truffle-hdwallet-provider");
const MNEMONIC = 'weather awesome name receive vacant stomach laugh margin else ritual chicken equip';

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*"
    },
    ropsten: {
      provider: function() {
        return new HDWalletProvider(MNEMONIC, "https://ropsten.infura.io/v3/a14f0ed4d3f749d0b79f545c921ed80c")
      },
      network_id: 3,
      gas: 4000000      //make sure this gas allocation isn't over 4M, which is the max
    }
  }
};
