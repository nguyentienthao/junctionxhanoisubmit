const Web3 = require('web3');
const fs = require('fs');
const program = require("commander");

module.exports = function() {

program
    .version("1.0.0")
    .option("-f, --function []", "Function name", "totalSupply")
    .option("--contract, --contract []", "Contract address")
    .option("--from, --from []", "From address")
    .option("--priv, --privatekey []", "From address's Private key")
    .option("--to, --to []", "To address")
    .option("--value, --value []", "Values")
    .option("--spender, --spender []", "Spender address")
    .option("--newOwner, --newOwner []", "newOwner address")
    .option("--transFromAdd, --transFromAdd []", "TransferFrom address" )    
    .parse(process.argv);

// web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/d7be9cfba89b4fb7adfd6960b57b7629d7be9cfba89b4fb7adfd6960b57b7629"));

web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));

var functionName = program.function;
var contractAddress = program.contract || '0x9326f741235033797f39b4e023b5dc4cdff2d80b';
var fromAddress = program.from || '0xd135E61d4B11cf4D9e3072A0DE537b116773B34B';
var fromAddressPrivateKey = program.privatekey || 'f2ff7ede21af852c6258ce3e4888412202bc19dec7faaafa0d24041627d1b9fa';
var toAddress = program.to || '0xDAE9Da7bd280465dc6a83c9C8A682060E9aa998d'; // account3
var spender = program.spender || '0xCC3c743d20B3eE230b20F3887F9afB1430403D32'; // account2

const contractABI = JSON.parse(fs.readFileSync('./build/contracts/KuramaToken.json')).abi;
const KuramaToken = new web3.eth.Contract(contractABI, contractAddress);
const contractUtils = require('./web3-contract-utils')(web3, KuramaToken);

var contractOwner // account0
// console.log(KuramaToken);
KuramaToken.methods.owner().call({}, function(error, result){
    if(!error){
        contractOwner = result;
        console.log(result);
        startTesting();
        return;
    }
    // console.log(error);
}); 

const startTesting = () => {
    const fromObj = {
        address: fromAddress || contractOwner,
        privateKey: fromAddressPrivateKey
    };
    const functions = {
        totalSupply() {
            contractUtils.totalSupply();
        },
        balanceOf(){
            contractUtils.balanceOf(fromAddress).then(console.log);
        },
        transfer() {
            let value = program.value || 10000;
            contractUtils.transfer(fromObj, toAddress, value, contractAddress).then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log(error);
            });
        },
        approve() {
            let value = program.value || 10000;
            contractUtils.approve(fromObj, spender, value, contractAddress).then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log(error);
            });
        },
        transferFrom() {
            let value = program.value || 10000;
            transferFromAddress = program.transFromAdd;
            contractUtils.transferFrom(fromObj, transferFromAddress, toAddress, value, contractAddress).then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log(error);
            });
        },
        allowance() {
            contractUtils.allowance(fromAddress, spender);   
        },
        burn() {
            let value = program.value || 0;
            contractUtils.burn(fromObj, value, contractAddress);
        },
        transferAnyERC20Token() {
            let value = program.value || 0;
            contractUtils.transferAnyERC20Token(fromObj, contractAddress, value)
        },
        getOwner() {
            contractUtils.getOwner();
        },
        unpause() {            
            contractUtils.unpause(fromObj, contractAddress);
        },
        pause() {
            contractUtils.pause(fromObj, contractAddress);
        },
        transferOwnership() {
            let newOwnerAddress = program.newOwner;
            contractUtils.transferOwnership(fromObj, newOwnerAddress, contractAddress);
        },
        acceptOwnership() {
            contractUtils.acceptOwnership(fromObj, contractAddress);
        }
    }

    switch (functionName) {
        case 'totalSupply':
            functions.totalSupply();
            break;
        case 'balanceOf':
            functions.balanceOf();
            break;
        case 'transfer':
            functions.transfer();
            break;
        case 'approve':
            functions.approve();
            break;
        case 'transferFrom':
            functions.transferFrom();
            break;
        case 'allowance':
            functions.allowance();
            break;
        case 'burn':
            functions.burn();
            break;
        case 'transferAnyERC20Token':
            functions.transferAnyERC20Token();
            break;                        
        case 'getOwner':
            functions.getOwner();
            break;                        
        case 'pause':
            functions.pause();
            break;                        
        case 'unpause':
            functions.unpause();
            break;                        
        case 'transferOwnership':
            functions.transferOwnership();
            break;                        
        case 'acceptOwnership':
            functions.acceptOwnership();
            break;                                  
    }
}
};