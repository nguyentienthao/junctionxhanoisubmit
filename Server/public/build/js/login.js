$(document).ready(function() {
	var server_post = "http://10.79.1.213:8080"
	$("#submit_login").click(function (e) {
		if($('#password').val() != "" && $('#user_name').val() != "" ){
			data_post = {
				"username": $('#user_name').val(),
				"password": $('#password').val()
			}
			// alert(data_post)
			// $(location).attr('href', 'agency.html')
			console.log(data_post)
			$.ajax({
				url: server_post+"/agency/login",
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json',
				success: function(data){
					console.log(data.user.UserID);
					$(location).attr('href','/agency/history/'+ String(data.user.UserID));
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					// alert("some error");
					$.notify({
						title: '<strong>Error:<br></strong>',
						message: "Can't Login"
					},{
						type: 'danger',
						offset:{
							x: 30,
							y: 40
						}
					});
				},
				data: JSON.stringify(data_post)
		});

		}else{
			$.notify({
				title: '<strong>Error:<br></strong>',
				message: "Please fill in all information"
			},{
				type: 'danger',
				offset:{
					x: 30,
					y: 40
				}
			})
		}

	})
	$("body").keypress(function (e) {
        if(e.which == 13){
            $('#submit_login').click();
        }
    });
	// Submit Login



});
