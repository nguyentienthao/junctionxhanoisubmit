var express = require('express');
var router = express.Router();
var path = require('path');
var util = require('util');
var os = require('os');
var passport = require("passport");
var mysql      = require('mysql');
var contractUtils = require('../libs/contract-utils')()
var Transaction = require('../models/Transaction.model');

var connection = require('../models/pool.connection')

// console.log(contractUtils);
router.post('/sendGift', function(req, res, next) {
  body = req.body;
  console.log(body);
  res.setHeader('Content-Type', 'application/json');
  getUserBalance(body.from,function () {
    if(userBalance < body.value){
      res.end(JSON.stringify({ "status": "customer not enough coin" }, null, 3));
      return
    }else{

      sendGift(body).then(function(results) {
        // contractUtils.transferFromTo(10**8).then(txhash => {
        //   console.log(txhash);
        // })
        // .catch(error => console.log());
        console.log("now  "+ Date.now());
        transaction = [[body.from, body.to, "0x6f9bdcd5c5d3f1b1a96b347ef08ffc43b03cc7a3784f066b93c844baf9a20a9d","123",Date.now()]];
        Transaction.addNewTransaction(transaction, (err, resTran) => {
         if(err) { throw (err) };
         console.log("res insert Transaction" + resTran);
        });
        res.send(JSON.stringify({ "status": "success sendGift" }, null, 3));
      }).catch((err) => setImmediate(() => {
        res.send(JSON.stringify({ "status": "fail to sendGift" }, null, 3));
        // throw err;
      }));
    }

  })


});


//createAgency for admin
router.get('/createCommunity/', function(req, res, next) {
  res.render('createCommunity', { title: 'createCommunity' });
});

router.post('/createCommunity', function(req, res, next) {
  console.log('--------------------start create comunity-------------------');
  communityInfo = req.body;
  res.setHeader('Content-Type', 'application/json');
  createCommunity(communityInfo).then(function(results) {
    res.send(JSON.stringify({ "status": "success create Community" }, null, 3));
  }).catch((err) => setImmediate(() => {
    res.send(JSON.stringify({ "status": err }, null, 3));
    // throw err;
  }));


});

//createAgency for admin
router.get('/createAgency/', function(req, res, next) {
  res.render('createAgency', { title: 'createAgency' });
});

router.post('/createAgency', function(req, res, next) {
  console.log('--------------------start create agency-------------------')
  agencyInfo = req.body;
  createAgency(agencyInfo).then(function(results) {
    res.send(JSON.stringify({ "status": "success create Agency" }, null, 3));
  }).catch((err) => setImmediate(() => {
    res.send(JSON.stringify({ "status": err }, null, 3));
    // throw err;
  }));
  res.setHeader('Content-Type', 'application/json');
});

function createAgency(agencyInfo){
  //create User
  console.log("createAgency1");

  return new Promise(function(resolve, reject) {
    insertUser = `INSERT INTO Users (Phone,Password,UserName,Role)
    VALUES ('`+agencyInfo.Phone+`','`+agencyInfo.Phone+`','`+ agencyInfo.userName+`', '`+agencyInfo.Role+`');`;
    console.log(insertUser);
    connection.query(insertUser, function (error, results, fields) {
      if (error) {
        return reject(error);
      }
      userID = results.insertId;
      insertAgency = `INSERT INTO Agencies (agencyName)
      VALUES ('`+agencyInfo.agencyName+`');`;
      //create Agency
      connection.query(insertAgency, function (error, results, fields) {
        if (error) {
          return reject(error);
        }
        resolve(results);
        contractUtils.allocateTokenToAgency(10**7).then(agency => {
          console.log("agency "+ util.inspect(agency, false, null, true));
          insertAddress = `INSERT INTO Addresses (AddreesEther,Value,Users_UserID,PrivateKey)
          VALUES ('`+agency.address+`','`+agencyInfo.balance+`','`+userID+`','`+agency.privateKey+`');`;
          console.log("agencyInfo.balance:  "+ insertAddress);
          connection.query(insertAddress, function (error, results, fields) {
            if (error) throw error;

          });
        }).catch(error => console.log());


      });

    });
  });
}

function createCommunity(communityInfo) {
  return new Promise(function(resolve, reject) {
    insertCommunity = `INSERT INTO Communities (CommunityName,Description,CoinName)
    VALUES ('`+communityInfo.communityName+`','`+communityInfo.description+`','`+communityInfo.coinName+`');`;
    //create Agency
    connection.query(insertCommunity, function (error, results, fields) {
      if (error) {
        // return error;
        return reject(error);
      }
      resolve(results);
    });
  });
}

function sendGift(info) {
  console.log(info);
  return new Promise(function(resolve, reject) {
    updateFrom = `UPDATE Addresses SET Value = Value -'`+info.value+`' WHERE Users_UserID='`+info.from+`';`;
    console.log(updateFrom);
    connection.query(updateFrom, function (error, results, fields) {
      if (error) reject(error);
      resolve(results);

    });
    updateTo = `UPDATE Addresses SET Value = Value + '`+info.value+`' WHERE Users_UserID='`+info.to+`';`;
    connection.query(updateTo, function (error, results, fields) {
      if (error) reject(error);
      resolve(results);

    });
  });
}

function getUserBalance(userID,_callback) {
  query=`SELECT Value FROM Addresses WHERE Users_UserID=` + userID +`;`;
  console.log(query);
  connection.query(query, function (error, results, fields) {
    if (error) throw error;
      console.log(results[0].Value);
      userBalance = results[0].Value;

    _callback();
  });
}
module.exports = router;
