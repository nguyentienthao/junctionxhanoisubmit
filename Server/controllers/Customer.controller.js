var poolConnection = require('../models/pool.connection');
var bcrypt = require('bcryptjs');
var User = require('../models/User.model');
var Customer = require('../models/Customer.model');

// exports.login = (req, res, next) => {
//   console.log('customer login');
//   console.log(req);
//   // console.log('current user: ', req.user);
//   if(!req.user) return res.json({errAuthen: 'Authenticate fail!!!'});
//   res.json({
//     status:'true',
//     user: req.user
//   });
// }
exports.getByCustomerName = (req,res,next) => {
  console.log('get infor customer');
  return new Promise((resolve, reject) => {
     Customer.getByCustomerName(req, (err, customerInfo) => {
      if(err) { reject(err) };
      resolve(customerInfo);
    });
  });
}

exports.getItemUsingToken = (req,res,next) => {
  // console.log('get item using token');
  return new Promise((resolve, reject) => {
     Customer.getItemUsingToken(req, (err, status) => {
      if(err) { reject(err) };
      resolve(status);
    });
  });
}

exports.logout = (req, res) => {
  req.logout();
  res.json({msg: 'You are out of my system!'});
}
