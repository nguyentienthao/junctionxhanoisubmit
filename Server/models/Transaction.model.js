var poolConnection = require('../models/pool.connection');

exports.getByUserId = (userId,offset, callback) => {
  poolConnection.getConnection((err, connection) => {
    if(err) { return console.log(err); }
    poolConnection.getConnection((err, connection) => {
      const sellectQuery = 'SELECT * FROM loyalty.Transactions WHERE FromId = ? OR Toid = ? order by CreatedDate DESC LIMIT ?,10 ;';
      console.log("userId" + userId);
      connection.query(sellectQuery, [userId,userId,offset], (err, results, fields) => {
        console.log(results);
        console.log(results.length);
        if(!results.length) { return callback({err_msg: 'can not find user'}, null) }
        callback(null, results);
      })
      connection.release();
    })
  })
}

exports.addNewTransaction = (transaction, callback) => {
  poolConnection.getConnection((err, connection) => {
    if(err) { return console.log(err); }
    poolConnection.getConnection((err, connection) => {
      const sellectQuery = 'INSERT INTO loyalty.Transactions (FromId, ToId, TxHash, Value, CreatedDate) VALUES ?';
      console.log(transaction);
      connection.query(sellectQuery, [transaction], (err, results, fields) => {
        console.log(results);
        if(!results.affectedRows) { return callback({err_msg: 'can not add new transaction'}, null) }
        callback(null, results);
      })
      connection.release();
    })
  })
}
