/**
 * Resize function without multiple trigger
 * 
 * Usage:
 * $(window).smartresize(function(){  
 *     // code here
 * });
 */
(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
      var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args); 
                timeout = null; 
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100); 
        };
    };

    // smartresize 
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');

	
	
// Sidebar
function init_sidebar() {
// TODO: This is some kind of easy fix, maybe we can improve this
var setContentHeight = function () {
	// reset height
	$RIGHT_COL.css('min-height', $(window).height());

	var bodyHeight = $BODY.outerHeight(),
		footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
		leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
		contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

	// normalize content
	contentHeight -= $NAV_MENU.height() + footerHeight;

	$RIGHT_COL.css('min-height', contentHeight);
};

  $SIDEBAR_MENU.find('a').on('click', function(ev) {
	  console.log('clicked - sidebar_menu');
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                $SIDEBAR_MENU.find('li ul').slideUp();
            }else
            {
				if ( $BODY.is( ".nav-sm" ) )
				{
					$li.parent().find( "li" ).removeClass( "active active-sm" );
					$li.parent().find( "li ul" ).slideUp();
				}
			}
            $li.addClass('active');

            $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

// toggle small or large menu 
$MENU_TOGGLE.on('click', function() {
		console.log('clicked - menu toggle');
		
		if ($BODY.hasClass('nav-md')) {
			$SIDEBAR_MENU.find('li.active ul').hide();
			$SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
		} else {
			$SIDEBAR_MENU.find('li.active-sm ul').show();
			$SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
		}

	$BODY.toggleClass('nav-md nav-sm');

	setContentHeight();

	$('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
});

	// check active menu
	$SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

	$SIDEBAR_MENU.find('a').filter(function () {
		return this.href == CURRENT_URL;
	}).parent('li').addClass('current-page').parents('ul').slideDown(function() {
		setContentHeight();
	}).parent().addClass('active');

	// recompute content when resizing
	$(window).smartresize(function(){  
		setContentHeight();
	});

	setContentHeight();

	// fixed sidebar
	if ($.fn.mCustomScrollbar) {
		$('.menu_fixed').mCustomScrollbar({
			autoHideScrollbar: true,
			theme: 'minimal',
			mouseWheel:{ preventDefault: true }
		});
	}
};



	   
	   
	$(document).ready(function() {
				
		init_sidebar();
		var server_post = "http://10.79.1.173:8080"

		// Submit create Community
		$("#submit_community").click(function (e) {
			if($('#community_name').val() != "" && $('#coin_name').val() != "" && $('#description').val() != ""){
				data_post = {
					"communityName": $('#community_name').val(),
					"coinName": $('#coin_name').val(),
					"description": $('#description').val()
				}
				// alert(data_post)
				console.log(data_post)
				$.ajax({
					url: server_post+"/admin/createCommunity",
					type: 'POST',
					dataType: 'json',
					contentType: 'application/json',
					success: function (data) {
							console.log("data response: " + JSON.stringify(data))
							if (data.status == "success create Community"){
								$.notify({
									title: '<strong>Success:<br></strong>',
									message: 'Created a new Community'
								},{
									type: 'success',
									offset:{
										x: 30,
										y: 60
									}
								});
							}else{
								$('#community_name').val("")
								$('#coin_name').val("")
								$('#description').val("")
								$.notify({
									title: '<strong>Error:<br></strong>',
									message: "Can't create a new Community"
								},{
									type: 'danger',
									offset:{
										x: 30,
										y: 60
									}
								});
							}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						// alert("some error");
						$.notify({
							title: '<strong>Error:<br></strong>',
							message: "Can't create a new Community"
						},{
							type: 'danger',
							offset:{
								x: 30,
								y: 40
							}
						});
					},
					data: JSON.stringify(data_post)
			});
			
			}else{
				$.notify({
					title: '<strong>Error:<br></strong>',
					message: "Please fill in all information"
				},{
					type: 'danger',
					offset:{
						x: 30,
						y: 40
					}
				})
			}

		})


		// Submit create Agency
		$("#submit_agency").click(function (e) {
			if($('#balance').val() != "" && $('#agency_name').val() != "" && $('#user_name').val() != "" 
			&& $('#role').val() != "" && $('#phone_number').val() != "" && $('#description').val() != "" ){
				data_post = {
					"balance": $('#balance').val(),
					"agencyName": $('#agency_name').val(),
					"userName": $('#user_name').val(),
					"Role": $('#role').val(),
					"Phone": $('#phone_number').val(),
					"description": $('#description').val(),

				}
				// alert(data_post)
				console.log(data_post)
				$.ajax({
					url: server_post+"/admin/createAgency",
					type: 'POST',
					dataType: 'json',
					contentType: 'application/json',
					success: function (data) {
							console.log("data response: " + JSON.stringify(data))
							if (data.status == "success create Agency"){
								$.notify({
									title: '<strong>Success:<br></strong>',
									message: 'Created a new Agency'
								},{
									type: 'success',
									offset:{
										x: 30,
										y: 60
									}
								});
							}else{
								$('#balance').val("")
								$('#agency_name').val("")
								$('#user_name').val("")
								$('#role').val("")
								$('#phone_number').val("")
								$('#description').val("")
								$.notify({
									title: '<strong>Error:<br></strong>',
									message: "Can't create a new Agency"
								},{
									type: 'danger',
									offset:{
										x: 30,
										y: 60
									}
								});
							}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						// alert("some error");
						$.notify({
							title: '<strong>Error:<br></strong>',
							message: "Can't create a new Agency"
						},{
							type: 'danger',
							offset:{
								x: 30,
								y: 40
							}
						});
					},
					data: JSON.stringify(data_post)
			});
			
			}else{
				$.notify({
					title: '<strong>Error:<br></strong>',
					message: "Please fill in all information"
				},{
					type: 'danger',
					offset:{
						x: 30,
						y: 40
					}
				})
			}

		})

		// Submit Login

		$("#submit_login").click(function (e) {
			if($('#password').val() != "" && $('#user_name').val() != "" ){
				data_post = {
					"user_name": $('#user_name').val(),
					"password": $('#password').val()
				}
				// alert(data_post)
				$(location).attr('href', 'community.html')
				console.log(data_post)
				$.ajax({
					url: server_post+"/admin/createCommunity",
					type: 'POST',
					dataType: 'json',
					contentType: 'application/json',
					success: function (data) {
							console.log("data response: " + JSON.stringify(data))
							if (data.status == "success create Community"){
								$.notify({
									title: '<strong>Success:<br></strong>',
									message: 'Created a new Community'
								},{
									type: 'success',
									offset:{
										x: 30,
										y: 60
									}
								});
							}else{
								$('#community_name').val("")
								$('#coin_name').val("")
								$('#description').val("")
								$.notify({
									title: '<strong>Error:<br></strong>',
									message: "Can't create a new Community"
								},{
									type: 'danger',
									offset:{
										x: 30,
										y: 60
									}
								});
							}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						// alert("some error");
						$.notify({
							title: '<strong>Error:<br></strong>',
							message: "Can't create a new Community"
						},{
							type: 'danger',
							offset:{
								x: 30,
								y: 40
							}
						});
					},
					data: JSON.stringify(data_post)
			});
			
			}else{
				$.notify({
					title: '<strong>Error:<br></strong>',
					message: "Please fill in all information"
				},{
					type: 'danger',
					offset:{
						x: 30,
						y: 40
					}
				})
			}

		})
				
	});	
	



