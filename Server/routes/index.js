var express = require('express');
var router = express.Router();
var QRCode = require('qrcode')

var path = require('path');
var util = require('util');
var os = require('os');

/* GET home page. */
router.get('/', function(req, res, next) {
  // console.log('render index');
  res.render('login', { title: 'Express' });
});

router.get('/testing', function(req, res, next) {
  console.log('render index');
  res.render('test', { title: 'Express' });
});

module.exports = router;
