var poolConnection = require('../models/pool.connection');
var bcrypt = require('bcryptjs');

exports.getById = (id, callback) => {
  poolConnection.getConnection((err, connection) => {
    if(err) { return console.log(err); }
    poolConnection.getConnection((err, connection) => {
      const sellectQuery = 'SELECT * FROM loyalty.Users WHERE UserID = ?;';
      connection.query(sellectQuery, [id], (err, results, fields) => {
        // console.log(results);
        // console.log('hehhe');
        const user = results[0];
        if(!user) { return callback({err_msg: 'can not find user'}, null) }
        callback(null, user);
      })
      connection.release();
    })
  })
}
exports.getByCustomerName = (username, callback) => {
  poolConnection.getConnection((err, connection) => {
    if(err) { return console.log(err); }
    poolConnection.getConnection((err, connection) => {
      const sellectQuery = 'SELECT * FROM loyalty.Users, loyalty.Customers WHERE loyalty.Users.UserName = ? and loyalty.Customers.UserID = loyalty.Users.UserID;';
      connection.query(sellectQuery, [username], (err, results, fields) => {
        // console.log(results);
        // console.log('hehhe');
        const user = results[0];
        if(!user) { return callback({err_msg: 'can not find user'}, null) }
        callback(null, user);
      })
      connection.release();
    })
  })
}

