const Web3 = require('web3');
const fs = require('fs');

const DEVELOPMENT = true;

if (DEVELOPMENT == false){
    web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/a14f0ed4d3f749d0b79f545c921ed80c"));
    contractAddress = '0xa5038773a41018e3a2ab1b370d885e8320fb44f1';

    supplyAddress = '0x0BDD18d619212a35FA7baFa835c90F8cFBf62b8A'; //account1
    privateKeySupplyAddr = '20711309AEA34063E5F9E98F27779BA3C8912C40B9EEB8300D7DED1860CAA318';

    supplyAddress2 = '0xd420b13102B9D22BC292FdA2B0822374C541F0AE'; //account3
    privateKeySupplyAddr2 = '715CBD70F7228D40DF38246CB20708F3254D6A9FCB5D37A208FF110E7A93ED20';

    spender = '0x0D9F4E96D1eD312ED0Dd60a92cB80484412d2a3b'; //account2
    privateKeySpender = '93DFB1DF2A416194FD7426D34819943DBBEB58EDA4A73AD41B6A14E99CDEE35F';

    account4 = '0x8A05276e51315C0b0be8229380929E2938178306'; //account4
}else{
    web3 = new Web3(new Web3.providers.HttpProvider("http://10.79.1.207:7545"));
    contractAddress = '0x9326f741235033797f39b4e023b5dc4cdff2d80b';

    supplyAddress = '0xd135E61d4B11cf4D9e3072A0DE537b116773B34B'; //account1
    privateKeySupplyAddr = 'f2ff7ede21af852c6258ce3e4888412202bc19dec7faaafa0d24041627d1b9fa';

    supplyAddress2 = '0xCC3c743d20B3eE230b20F3887F9afB1430403D32'; //account3
    privateKeySupplyAddr2 = 'b0dd29230a70e84a2f6f800ec34a74dd4d697d5463cf90e595b865a275801712';

    spender = '0xDAE9Da7bd280465dc6a83c9C8A682060E9aa998d'; //account2
    privateKeySpender = '640dd8563314519c5f2ccd79d42d84b2cd93918237ec7597431ba3582e706f86';

    account4 = '0x6338C8BC5fe27A0072a7EE4fbE264310C9237d6a'; //account4
}

const contractABI = JSON.parse(fs.readFileSync('./smart_contract/truffle/build/contracts/KuramaToken.json')).abi;
const KuramaToken = new web3.eth.Contract(contractABI, contractAddress);

const web3ContractUtils = require('./web3-contract-utils.js')(web3, KuramaToken);
const infinitoContractUtils = require('./infinito-contract-utils')();


module.exports = function(){
    const utils = {
        allocateTokenToAgency(value){
            let agencyAcc = infinitoContractUtils.createNewAddress(1)[0];
  //           agencyAcc = { privateKey: 'd6420a5895356b5da742a5f1cecb3441b378b411ad795732fa673621a98954a5',
  // address: '0x4f030a0926f080716ea77e9fcb56e3047c3ce32a' };
            console.log(agencyAcc)

            // send some ether from Sup2
            let someEther = 0.3 * 10**18;
                if (DEVELOPMENT == false){
                    infinitoContractUtils.transferEther('0x'+privateKeySupplyAddr2, agencyAcc.address, someEther)
                    .then(result => {
                        console.log(result);
                    }).catch(error => {
                        console.log(error);
                    });
                }else{
                    web3ContractUtils.transferEther({
                        address: supplyAddress2,
                        privateKey: privateKeySupplyAddr2
                    }, agencyAcc.address, {value: someEther})
                    .then(result => {
                        console.log(result);
                    }).catch(error => {
                        console.log(error);
                    });
                }

            return new Promise((resolve, reject) => {
                // send tokens
                let fromObj = {
                    address: supplyAddress,
                    privateKey: privateKeySupplyAddr
                }
                web3ContractUtils.transfer(fromObj, agencyAcc.address, value, contractAddress).then(result => {
                    agencyAcc.txHash = result.transactionHash;
                    resolve(agencyAcc);
                }).catch(error => {
                    if(error) reject(error);
                });
            });
        },
        agencyCreateAndAddToken(agency, users){
            if(users.length){
                let accounts = infinitoContractUtils.createNewAddress(users.length);
                for(let count = 0; count < accounts.length; count++){
                    let value = users[count].value;
                    let id = users[count].id;
                    accounts[count].value = value;
                    accounts[count].id = id;
                }
                this.givingEtherToAddr(accounts).then(results => {
                    console.log("givingEtherToAddr", results);
                }).catch(error => console.log(error));
                return this.agencyAddTokenToExistedAddr(agency, accounts);
                // return new Promise((resolve, reject) => {
                //     this.agencyAddTokenToExistedAddr(agency, accounts).then(results => {
                //         console.log("agencyAddTokenToExistedAddr", results);
                //     }).catch(error => console.log(error));
                // });
            }
            return false;
        },
        givingEtherToAddr(accounts){
            // send some ether if created
            var someEther = 0.1 * 10**18;

            if(DEVELOPMENT == false){
                return Promise.all([infinitoContractUtils.transferEther('0x'+privateKeySupplyAddr, accounts[0].address, someEther), infinitoContractUtils.transferEther('0x'+privateKeySupplyAddr2, accounts[1].address, someEther)])
                .then(results => {
                    return results;
                })
                // infinitoContractUtils.transferEther('0x'+privateKeySupplyAddr, accounts[0].address, someEther)
                // .then(result => {
                //     count++;
                //     console.log(result);
                // }).catch(error => {
                //     console.log(error);
                //     count++;
                // });
                // infinitoContractUtils.transferEther('0x'+privateKeySupplyAddr2, accounts[1].address, someEther)
                // .then(result => {
                //     count++;
                //     console.log(result);
                // }).catch(error => {
                //     console.log(error);
                //     count++;
                // });
            }else{
                let fromObj1 = {
                    address: supplyAddress,
                    privateKey: privateKeySupplyAddr
                };
                let fromObj2 = {
                    address: supplyAddress2,
                    privateKey: privateKeySupplyAddr2
                }
                return Promise.all([web3ContractUtils.transferEther(fromObj1, accounts[0].address, {value: someEther}), web3ContractUtils.transferEther(fromObj2, accounts[1].address, {value: someEther})])
                .then(results => {
                    return [results[0].transactionHash, results[1].transactionHash]
                });
            }
        },
        agencyAddTokenToExistedAddr(agencyAcc, accounts){
            if(accounts.length){
                // var errorStatus = [];
                return new Promise((resolve, reject) => {
                    results = [];
                    this.transferFromTo(agencyAcc, accounts[0].address, accounts[0].value, contractAddress).then(tx => {
                        accounts[0].txHash = tx;
                        this.transferFromTo(agencyAcc, accounts[1].address, accounts[1].value, contractAddress)
                        .then(tx => {
                            accounts[1].txHash = tx;
                            resolve(accounts);
                        });
                    });
                });
                // return Promise.all([])
                // .then(this.transferFromTo(agencyAcc, accounts[1].address, accounts[1].value, contractAddress)
                // ).then(results => {
                //     return results;
                // });
                // var count = 0;
                // while (count < accounts.length){
                //     var account = accounts[count];
                //     // tranfer token
                //     this.transferFromTo(agencyAcc, account.address, account.value, contractAddress)
                //     .then(result => {
                //         console.log("transferFromTo", count, result);
                //         count++
                //         errorStatus.push({
                //             id: account.id,
                //             status: true
                //         });
                //         if(count == accounts.length -1) return errorStatus;
                //     }).catch(error => {
                //         console.log(error);
                //         count++
                //         errorStatus.push({
                //             id: account.id,
                //             status: false
                //         });
                //         if(count == accounts.length -1) return errorStatus;
                //     });
                // }
            }
            return false;
        },
        transferFromTo(fromObj, toAddress, value, contractAddress) {
            return new Promise((resolve, reject) => {
                web3ContractUtils.transfer(fromObj, toAddress, value, contractAddress)
                .then(result => {
                    resolve(result.transactionHash);
                }).catch(error => reject(error));
            });
        },
        waiting(ms){
            //waiting ms
            var start = new Date().getTime();
            var end = start;
            while(end < start + ms) {
                end = new Date().getTime();
            }
        }
    };
    let users = [
        {
            id: 1,
            value: 1000
        },
        {
            id: 3,
            value: 5000
        }
    ];
    fromObj = {
        address: supplyAddress,
        privateKey: privateKeySupplyAddr
    }
    // web3ContractUtils.balanceOf(supplyAddress).then(console.log).catch(console.log)
    // web3ContractUtils.getNonce(spender).then(console.log).catch(console.log)

  //   agencyAcc = { privateKey: 'd6420a5895356b5da742a5f1cecb3441b378b411ad795732fa673621a98954a5',
  // address: '0x4f030a0926f080716ea77e9fcb56e3047c3ce32a' };
  //   var addresses = utils.agencyCreateAndAddToken(agencyAcc, users);
  //   console.log(addresses);
    // utils.allocateTokenToAgency(10**7).then(result => console.log(result))
    // .catch(error => console.log(error));
    // utils.transferFromTo(fromObj, account4, 1000, contractAddress).then(result => console.log(result))
    // .catch(error => console.log(error));


    return utils;
}

// infinitoContractUtils.transfer(privateKey, toAddr, value, contractAddress)
// .then(result => {
//     console.log(123123123123, result);
// });

// infinitoContractUtils.approve(privateKey, spender, value, contractAddress)
// .then(result => {
//     console.log(123123123123, result);
// });


// fromObj = {
//     address: "0xd420b13102B9D22BC292FdA2B0822374C541F0AE",
//     privateKey: privateKey
// }


// infinitoContractUtils.transferFrom(fromObj, toAddr, value, contractAddress)
// .then(result => {
//     console.log(123123123123, result);
// });

// infinitoContractUtils.totalSupply(contractAddress)
// .then(result => {
//     console.log(123123123123, result);
// });
